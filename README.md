# olip-deploy

[![pipeline status](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/badges/master/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/commits/master)

See installation instructions at:
<http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/olip/installation/>

## Description

This playbook installs the OLIP stack and its components
(backend, frontend, routing, stats).

## Contributing

### TM/madrid specifics

The VMs are built using the `olippp` image,
which runs the playbook with the tags `olip` and `develop`.

The run plays all the tasks until the `api` and `dashboard` containers are created,
which is everything it needs to move on content installation.

Anything before must carefully make use of tags and `when:` conditionnals
in order to not fail because of the madrid context.
