#!/bin/bash

# bibliosansfrontieres/olip/olip-deploy#68
todo_count=$( sqlite3 /data/app.db "select count(*) from installed_contents where target_state != current_state ;" )
if [[ "$todo_count" -ne 0 ]] ; then
    echo >&2 "Contents install queue is not empty (${todo_count} remaining), not updating the catalog."
    exit 0
fi

UPDATE_DESCRIPTOR_URL="http://localhost:5002/applications/?visible=true&repository_update=true&from=systemd.service"

curl --silent -X GET "$UPDATE_DESCRIPTOR_URL" > /dev/null
rc=$?

if [[ "$rc" -eq 0 ]] ; then
    echo "Catalog updated."
else
    echo "Failed to update the catalog (return code: $rc)."
fi

exit "$rc"
