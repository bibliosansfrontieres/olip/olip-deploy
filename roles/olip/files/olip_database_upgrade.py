#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script handles the process_id deployments as defined within TM.

It checks whether we're online, then asks TM for a job.

If a job is found, it compares the process_id to be deployed with the previously deployed process_id.

If they differ, it compares the contents in order to keep the ones that are equel between both process_ids,
and remove the ones that differ.

Its operations are pushed to TM's logs for our users to follow the deployment progress.
"""

import json
import socket
import sqlite3
import sys
import tarfile
from datetime import datetime
from os import path, remove
from pathlib import Path
from shutil import copyfile, rmtree
from time import sleep

import docker
import psutil
import requests
from pid import PidFile, PidFileAlreadyLockedError


requests.packages.urllib3.disable_warnings(
    requests.packages.urllib3.exceptions.InsecureRequestWarning
)

class OlipUpgrade:
    def __init__(self):
        self.tm_api = (
            "https://front.tm.bsf-intranet.org/goto/temps-modernes/api/deploy/job/"
        )
        self.tm_log = "https://front.tm.bsf-intranet.org/goto/temps-modernes/api/logs/"
        self.api_url = "http://localhost:5002"
        self.olip_storage_path = "/data/"
        self.olip_configfile = Path("/etc/default/olip")
        self.target_olip_db = "app.db"
        self.origin_olip_db = "app.db.original"
        self.olip_data_from_configfile = {}
        self.mac_address = str()
        self.ip_address = str()
        self.vpn_interface_name = "testvpn"
        self.vpn_mac_address = str()
        self.vpn_ip_address = str()
        self.client = docker.DockerClient(base_url="unix://var/run/balena-engine.sock")
        self.all_interfaces_names = psutil.net_if_addrs()

    def logger(self, level, message):
        """
        Logger plugged to TM

        Args:
            str: level (DEBUG, INFO, ERROR)
            str: message
        """

        print("[+] {}: {}".format(level, message))

        data = {
            "log": self.mac_address[-5:] + " - " + message,
            "level": level,
            "process_id": self.get_process_id(),
            "user": self.mac_address,
        }

        requests.post(
            self.tm_log,
            json=data,
            headers={"Content-Type": "application/json"},
            verify=False,
        )

    def internet_is_up(self):
        """
        Checks whether internet is accessible by trying to connect to 8.8.8.8:80
        Populate MAC addresses variables
        """
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            s.getsockname()[0]

            for interface in self.all_interfaces_names:
                if (
                    s.getsockname()[0]
                    == self.all_interfaces_names[interface][0].address
                ):
                    nic = psutil.net_if_addrs().get(interface)

                    self.mac_address = [x[1] for x in nic if "AF_PACKET" in str(x)][0]
                    self.ip_address = [x[1] for x in nic if "AF_INET" in str(x)][0]

                if interface == self.vpn_interface_name:
                    nic = psutil.net_if_addrs().get(interface)

                    self.vpn_mac_address = [x[1] for x in nic if "AF_PACKET" in str(x)][
                        0
                    ]
                    self.vpn_ip_address = [x[1] for x in nic if "AF_INET" in str(x)][0]

            s.close()
            print("[+] DEBUG: Internet is up")
            return True
        except:  # noqa E722
            pass
        return False

    def read_data_from_configfile(self):
        """
        Reads key/value pairs from a bash-like file.

        Return:
            dict: key/values
        """

        if self.olip_configfile.is_file():
            print(
                "[+] DEBUG: Read configuration from {}...".format(self.olip_configfile)
            )
            data = {}
            with open(str(self.olip_configfile)) as f:
                for line in f:
                    name, var = line.partition("=")[::2]
                    data[name.strip()] = var.strip()
            self.olip_data_from_configfile = data
        else:
            self.olip_data_from_configfile = None

    def get_project_name(self):
        """
        Returns:
            str: project name to be deployed (eg: idc-bsf-someproject)
        """
        project_name = self.olip_data_from_configfile["target_project_name"]
        return project_name

    def get_process_id(self):
        """
        Returns:
            str: process_id to be deployed
        """
        process_id = self.olip_data_from_configfile["target_process_id"]
        return process_id

    def stop_containers(self):
        """Stops API container and all Apps containers"""
        try:
            olip_container = self.client.containers.get("api")
            self.logger("DEBUG", "Stopping API container...")
            olip_container.stop()

            webapps = self.client.containers.list(
                filters={"status": "running", "name": "app"}
            )

            for webapp in webapps:
                self.logger("DEBUG", "Stopping {} container...".format(webapp.name))
                webapp.stop()

            self.logger("DEBUG", "Olip api container and WebApp stopped")

        except docker.errors.NotFound as e:
            self.logger("ERROR", "Container not found " + e)
            sys.exit(1)
        except docker.errors.APIError as e:
            self.logger("ERROR", "Docker unreachable " + e)
            sys.exit(1)

    def start_container(self):
        """Starts the API container and triggers a catalog update"""
        try:
            olip_container = self.client.containers.get("api")
            olip_container.start()

            self.logger("DEBUG", "Olip api container started")

        except docker.errors.NotFound as e:
            self.logger("ERROR", "Container not found " + e)
            sys.exit(1)
        except docker.errors.APIError as e:
            self.logger("ERROR", "Docker unreachable " + e)
            sys.exit(1)

        sleep(15)
        requests.get(
            self.api_url + "/applications/?visible=true&repository_update=true",
            verify=False,
        )

    def need_upgrade(self):
        """
        Does not trigger database upgrade if both current
        and target process ids are the same. 
        Triggers otherwise.

        Target process id is required.
        Current process id is optional.

        Return:
            bool: True/False
        """
        current_process_id = self.olip_data_from_configfile.get("current_process_id")
        target_process_id = self.olip_data_from_configfile.get("target_process_id")

        if target_process_id == None:
            error = "OLIP database upgrade cannot find target process id in /etc/default/olip"
            self.logger(
                "ERROR",
                error,
            )
            raise TypeError(error)

        if current_process_id == target_process_id:
            message = "process_id {} is already deployed, nothing to do.".format(current_process_id)
            upgrade = False
        else:
            message = "The OLIP database needs to be upgraded from {} to {}".format(current_process_id, target_process_id)
            upgrade = True

        self.logger(
                "INFO",
                message,
                )
        return upgrade

    def delete_content_from_filesystem(self, app, package_content_id):
        """
        Removes the content files.
        Also removes the build from contents if any.

        Args:
            str: app handling the content
            str: content_id
        """
        try:
            # path : /data/kiwix.app/content/package1
            path_to_content = Path(
                path.join(
                    self.olip_storage_path,
                    app + ".app",
                    "content",
                    package_content_id,
                )
            )
            if path_to_content.is_file():
                remove(str(path_to_content))
                self.logger(
                    "DEBUG",
                    "File " + package_content_id + " has been removed from filesystem",
                )
            elif path_to_content.is_dir():
                rmtree(str(path_to_content))
                self.logger(
                    "DEBUG",
                    "Directory "
                    + package_content_id
                    + " has been removed from filesystem",
                )

            # path : /data/mediacenter.app/package1
            path_to_build = Path(
                path.join(
                    self.olip_storage_path,
                    app + ".app",
                    package_content_id,
                )
            )
            if path_to_build.is_file():
                remove(str(path_to_build))
                self.logger(
                    "DEBUG",
                    "Build File "
                    + package_content_id
                    + " has been removed from filesystem",
                )
            elif path_to_build.is_dir():
                rmtree(str(path_to_build))
                self.logger(
                    "DEBUG",
                    "Build directory for "
                    + package_content_id
                    + " has been removed from filesystem",
                )

        except FileNotFoundError as e:
            self.logger(
                "ERROR",
                "unable to remove content {}: {}".format(package_content_id, str(e)),
            )
            self.logger("DEBUG", "Restarting api container anyway...")
            ideascube.start_container()
            sys.exit(1)

    def update_db(self):
        """
        The Big Deal.

        Prepares the target DB:
        - remove containers, users sessions
        - sets all installed_apps to current_state=uninstalled
        - sets all installed_contents to current_state=uninstalled

        Compares contents state in current/target DBs
        and for each of these,
            if different:
            - if not in target DB, delete from DB and filesystem
            - if in target DB:
                - if version differs, sets current_state=uninstalled to force reinstall
                - if target_state differs, delete from filesystem
            if identical:
            - sets current_state back to installed
        """

        con_to_target_db = sqlite3.connect(
            path.join(self.olip_storage_path, self.target_olip_db)
        )
        con_to_origin_db = sqlite3.connect(
            path.join(self.olip_storage_path, self.origin_olip_db)
        )

        cur_to_target_db = con_to_target_db.cursor()
        cur_to_origin_db = con_to_origin_db.cursor()

        con_to_target_db.executescript(
            """
            DELETE FROM containers;
            DELETE FROM tokens;
            UPDATE installed_applications SET current_state='uninstalled';
            UPDATE installed_contents SET current_state='uninstalled' \
            WHERE current_state='installed';
            """
        )

        target_data_row = set(
            cur_to_target_db.execute(
                "SELECT content_id, current_version, endpoint_container FROM installed_contents \
            WHERE target_state = 'installed'"
            ).fetchall()
        )
        origin_data_row = set(
            cur_to_origin_db.execute(
                "SELECT content_id, current_version, endpoint_container FROM installed_contents \
            WHERE target_state = 'installed'"
            ).fetchall()
        )

        difference_between_origin_target = origin_data_row.difference(target_data_row)

        self.logger(
            "INFO",
            str(len(difference_between_origin_target))
            + " package(s) differ (this doesn't mean what you think)",
        )

        for package in difference_between_origin_target:
            package_content_id = package[0]
            package_app = package[2]

            (
                package_from_origin_target_state,
                package_from_origin_current_version,
            ) = cur_to_origin_db.execute(
                "SELECT target_state, current_version FROM \
                    installed_contents WHERE content_id=?",
                (package_content_id,),
            ).fetchone()
            (
                package_from_target_target_state,
                package_from_target_target_version,
            ) = cur_to_target_db.execute(
                "SELECT target_state, target_version FROM \
                    installed_contents WHERE content_id=?",
                (package_content_id,),
            ).fetchone()

            self.logger("INFO", package_content_id + " differs")

            if package_from_target_target_version:
                self.logger(
                    "DEBUG",
                    "Previous state was "
                    + package_from_origin_target_state
                    + " and version number was "
                    + package_from_origin_current_version,
                )
                self.logger(
                    "DEBUG",
                    "Target state is "
                    + package_from_target_target_state
                    + " and version number is "
                    + package_from_target_target_version,
                )

                if (
                    package_from_origin_target_state != package_from_target_target_state
                    and package_from_target_target_state
                ):
                    self.logger(
                        "INFO",
                        package_content_id
                        + " has been removed in target db, deleting from filesystem...",
                    )
                    self.delete_content_from_filesystem(package_app, package_content_id)

                elif (
                    package_from_origin_current_version
                    != package_from_target_target_version
                    and package_from_target_target_version
                ):
                    self.logger(
                        "INFO",
                        package_content_id
                        + " has been updated in target db, delete folder on filesystem, mark current_state=uninstalled to be re-installed",
                    )

                    self.delete_content_from_filesystem(package_app, package_content_id)
                    con_to_target_db.execute(
                        "UPDATE installed_contents SET current_state='uninstalled' \
                        WHERE content_id=?",
                        (package_content_id,),
                    )
            else:
                self.logger(
                    "INFO", package_content_id + " is not in target db, deleting..."
                )

                self.delete_content_from_filesystem(package_app, package_content_id)

        packages_present_in_origin_target = origin_data_row.intersection(
            target_data_row
        )

        for package in packages_present_in_origin_target:
            package_content_id = package[0]
            package_app = package[2]

            # path : /data/mediacenter.1app/content/package1
            path_to_content = Path(
                path.join(
                    self.olip_storage_path,
                    package_app + ".app",
                    "content",
                    package_content_id,
                )
            )

            if not path_to_content.is_file() and not path_to_content.is_dir():
                self.logger(
                    "INFO",
                    package_content_id
                    + " is in target db but absent from filesystem, mark for installation",
                )
            else:
                self.logger(
                    "INFO",
                    package_content_id
                    + " is in target db and present on filesystem, nothing to do",
                )
                con_to_target_db.execute(
                    "UPDATE installed_contents SET current_state='installed', target_state='installed' \
                    WHERE content_id=?",
                    (package_content_id,),
                )

        con_to_target_db.commit()
        con_to_target_db.close()
        con_to_origin_db.close()

        self.logger("INFO", "Database updated with success")

    def remove_playbooks_flag_files(self):
        """
        Remove playbooks flag files in order to force their execution
        We want these to run again in order to replicate the project_name change if any
        """
        self.logger(
            "DEBUG", "Remove OLIP ansible playbook flags if any to rename device"
        )
        olip_playbook = ["olip-pre-install", "olip-deploy"]
        for playbook in olip_playbook:
            path_to_playbook_flag = Path(path.join("/opt", playbook))
            if path_to_playbook_flag.is_file():
                self.logger("DEBUG", "Removing " + str(path_to_playbook_flag))
                remove(str(path_to_playbook_flag))
            else:
                self.logger("DEBUG", str(path_to_playbook_flag) + " is absent")

    def update_configfile(self):
        """
        Write the new process_id and project_name in the config file
        and drop the old keys if needed
        """
        self.logger("DEBUG", "Updating config file")
        self.olip_data_from_configfile.update(
            {
                "current_process_id": self.get_process_id(),
                "target_process_id": self.get_process_id(),
                "current_project_name": self.get_project_name(),
                "target_project_name": self.get_project_name(),
            }
        )
        # remove old keys
        if "process_id" in self.olip_data_from_configfile:
            del self.olip_data_from_configfile["process_id"]
        if "project_name" in self.olip_data_from_configfile:
            del self.olip_data_from_configfile["project_name"]
        filedata = str()
        for key in self.olip_data_from_configfile:
            filedata += "{}={}\n".format(key, self.olip_data_from_configfile[key])
        with open(str(self.olip_configfile), "w") as f:
            f.write(filedata)

    def count_data_to_handle(self):
        """
        Counts Apps and Contents to handle

        Returns:
            tuple: contents to handle, apps to handle
        """

        con_to_target_db = sqlite3.connect(
            "file:"
            + path.join(self.olip_storage_path, self.target_olip_db)
            + "?mode=ro",
            uri=True,
        )

        cur_to_target_db = con_to_target_db.cursor()

        try:
            count_contents_to_handle = cur_to_target_db.execute(
                "SELECT count(*) from installed_contents \
            WHERE (current_state != target_state or current_version != target_version) and target_state != 'broken'"
            ).fetchone()
        except sqlite3.OperationalError as e:
            # database is locked
            count_contents_to_handle = (-1,)
        try:
            count_applications_to_handle = cur_to_target_db.execute(
                "SELECT count(*) from installed_applications \
            WHERE current_state != target_state or current_version != target_version"
            ).fetchone()
        except sqlite3.OperationalError as e:
            # database is locked
            count_applications_to_handle = (-1,)

        con_to_target_db.close()

        return (count_contents_to_handle[0], count_applications_to_handle[0])

    def send_remaining_data_to_handle_to_tm(self):
        """Posts the count of Apps and Contents to handle to the process_id log queue in TM"""

        previous_contents_count = 999999
        previous_applications_count = 999999

        while previous_contents_count > 0:
            sleep(60)
            (
                current_contents_count,
                current_applications_count,
            ) = self.count_data_to_handle()

            if current_contents_count == -1 or current_applications_count == -1:
                # database was locked, maybe more luck next time
                continue

            if (
                previous_contents_count != current_contents_count
                or previous_applications_count != current_applications_count
            ):
                previous_contents_count = current_contents_count
                previous_applications_count = current_applications_count
                self.logger(
                    "INFO",
                    str(current_contents_count)
                    + " contents and "
                    + str(current_applications_count)
                    + " applications to handle",
                )

        self.logger("INFO", "Last content finish its installation")
        sys.exit(0)

    def _convert_bytes(self, bytes):
        """Not Invented Here Syndrom"""
        bytes = float(bytes)
        if bytes >= 1099511627776:
            terabytes = bytes / 1099511627776
            size = "%.1fT" % terabytes
        elif bytes >= 1073741824:
            gigabytes = bytes / 1073741824
            size = "%.1fG" % gigabytes
        elif bytes >= 1048576:
            megabytes = bytes / 1048576
            size = "%.1fM" % megabytes
        elif bytes >= 1024:
            kilobytes = bytes / 1024
            size = "%.1fK" % kilobytes
        else:
            size = "%.1fb" % bytes
        return size

    def log_device_info(self):
        """Logs device info: IP addresses, Disk usage"""

        if self.vpn_interface_name in self.all_interfaces_names:
            self.logger(
                "INFO",
                "VPN info: "
                + self.vpn_ip_address
                + " / "
                + self.vpn_mac_address
                + " IP info: "
                + self.ip_address
                + " / "
                + self.mac_address,
            )
        else:
            self.logger(
                "INFO", "IP info: " + self.ip_address + " - " + self.mac_address
            )

        self.logger(
            "INFO",
            "Space available -> / "
            + str(self._convert_bytes(psutil.disk_usage("/").free))
            + ", /data/ "
            + str(self._convert_bytes(psutil.disk_usage("/data/").free)),
        )

    def backup_olip_database(self):
        """
        Saves the current database file
        """

        database_file_path = Path(self.olip_storage_path + self.target_olip_db)

        if database_file_path.is_file():
            try:
                ts = datetime.now().strftime("%Y%m%d%H%M%S")
                self.origin_olip_db = (
                    self.target_olip_db
                    + "-"
                    + ts
                    + "-"
                    + self.olip_data_from_configfile["current_process_id"]
                )

                copyfile(
                    self.olip_storage_path + self.target_olip_db,
                    self.olip_storage_path + self.origin_olip_db,
                )

                self.logger(
                    "INFO", "Backup Olip database with name " + self.origin_olip_db
                )

            except Exception as e:
                self.logger("ERROR", "Error while copying database: " + e)
                self.logger("DEBUG", "Restarting api container anyway...")
                ideascube.start_container()
                sys.exit(1)

    def download_olip_database(self):
        """
        Downloads the database file from TM
        """

        url = (
            "https://s3.eu-central-1.wasabisys.com/olip-db/"
            + self.get_project_name()
            + "/olip/"
            + self.get_process_id()
            + "/srv.tar"
        )
        tmp_archive_path = "/tmp/srv.tar"

        try:
            r = requests.get(url)
            r.raise_for_status()

            with open(tmp_archive_path, "wb") as db_file:
                db_file.write(r.content)
        except requests.exceptions.RequestException as e:
            self.logger(
                "ERROR",
                "Database is not available on Internet: {} ({})".format(
                    str(e),
                    url)
                )
            self.logger("DEBUG", "Restarting api container anyway...")
            ideascube.start_container()
            sys.exit(1)

        with tarfile.open(tmp_archive_path) as tar:
            tar.extract(self.target_olip_db, self.olip_storage_path)

        self.logger("INFO", "Database downloaded with success")


if __name__ == "__main__":
    try:
        with PidFile("olip_database_upgrade.pid", piddir="/var/run/") as p:
            ideascube = OlipUpgrade()

            if not ideascube.internet_is_up():
                sys.exit(0)

            ideascube.read_data_from_configfile()

            if ideascube.need_upgrade():
                ideascube.log_device_info()
                if ideascube.olip_data_from_configfile.get("current_process_id"):
                    ideascube.stop_containers()
                    ideascube.backup_olip_database()
                    ideascube.download_olip_database()
                    ideascube.update_db()
                    ideascube.update_configfile()
                    ideascube.remove_playbooks_flag_files()
                    ideascube.start_container()
                else:
                    ideascube.stop_containers()
                    ideascube.download_olip_database()
                    ideascube.update_configfile()
                    ideascube.start_container()
                ideascube.logger(
                    "INFO",
                    "Provisionning is done. Hurray! OLIP will install its contents...",
                )

                ideascube.send_remaining_data_to_handle_to_tm()

    except PidFileAlreadyLockedError:
        print("Another instance of olip_database_upgrade is already running")
        sys.exit(0)
