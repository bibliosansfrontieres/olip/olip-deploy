#!/bin/bash

balena-engine run \
    --name rpi-csv-stats \
    -e "INTERVALL=${INTERVAL:-1000}" \
    -v /data/logs/rpi-csv-stats:/logs \
    offlineinternet/rpi-csv-stats:latest
