#!/bin/bash

container_state=$( curl -s --unix-socket /var/run/balena-engine.sock 'http:/localhost/containers/json' \
                | jq -r '.[] | select(.Names[]=="/mediacenter.app.mediacenter") | .State' )
[ "$container_state" != "running" ] && {
    >&2 echo "Error: container mediacenter.app.mediacenter is not running."
    exit 1
}

>&2 echo "Triggering the mediacenter rebuild..."

balena-engine exec -it mediacenter.app.mediacenter hugo-build
