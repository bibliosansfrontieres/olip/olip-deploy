#!/bin/bash

max_battery="100"
min_battery="10"
current_state="empty"

function trigger_battery_led()
{
    local state=$1
    echo "Battery state: $state"
    current_state="$state"

    if [ "$state" == "charged" ]
    then
        echo default-on > /sys/class/leds/led1/trigger
    fi

    if [ "$state" == "charging" ]
    then
        echo timer > /sys/class/leds/led1/trigger
        echo 1000 > /sys/class/leds/led1/delay_on
    fi

    if [ "$state" == "discharging" ]
    then
        echo heartbeat > /sys/class/leds/led1/trigger
    fi

    if [ "$state" == "empty" ]
    then
        echo timer > /sys/class/leds/led1/trigger
        echo 200 > /sys/class/leds/led1/delay_on
        echo 200 > /sys/class/leds/led1/delay_off
    fi
}

function ask_pisugar()
{
    local cmd=$1
    echo "$cmd" | nc -q 0 127.0.0.1 8423
}

while true; do
    battery=$(ask_pisugar "get battery" | grep -o '[0-9\.]*$')
    battery_power_plugged=$(ask_pisugar "get battery_power_plugged" | grep -o -E '(true|false)$')

    if [[ ${battery%.*} -ge $max_battery && $battery_power_plugged == "true" ]]
    then
        state="charged"
    else
        state="discharging"
    fi

    if [[ ${battery%.*} -lt $max_battery && $battery_power_plugged == "true" ]]
    then
        state="charging"
    fi

    if [[ ${battery%.*} -lt $max_battery && ${battery%.*} -gt $min_battery && $battery_power_plugged == "false" ]]
    then
        state="discharging"
    fi

    if [[ ${battery%.*} -lt $min_battery && $battery_power_plugged == "false" ]]
    then
        state="empty"
    fi

    if [ "$current_state" != "$state" ] 
    then
        trigger_battery_led $state
    fi

    sleep 2
done
