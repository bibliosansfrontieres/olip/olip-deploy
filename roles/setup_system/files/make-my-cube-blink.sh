#!/bin/bash

# Quick'n'dirty PoC, ChatGPT Driven Development
#
# This script is intented to be triggered manually
# in order to visually find the device among others
#   ssh <device> make-my-cube-blink.sh crazy 50
#
# Related to https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/-/issues/58

function trigger_led()
{
    local blink_type="$1"
    local num_blinks="$2"

    if [ "$blink_type" == "long" ]
    then
        echo "Triggering long blink for $num_blinks times"
        for ((i = 0; i < num_blinks; i++))
        do
            echo 0 > /sys/class/leds/led1/brightness # Turn off
            sleep 0.5
            echo 1 > /sys/class/leds/led1/brightness # Turn on
            sleep 0.5
        done
    elif [ "$blink_type" == "short" ]
    then
        echo "Triggering short blink for $num_blinks times"
        for ((i = 0; i < num_blinks; i++))
        do
            echo 0 > /sys/class/leds/led1/brightness # Turn off
            sleep 0.2
            echo 1 > /sys/class/leds/led1/brightness # Turn on
            sleep 0.2
        done
        echo 0 > /sys/class/leds/led1/brightness # Turn off after short blinks
    elif [ "$blink_type" == "crazy" ]
    then
        echo "Triggering crazy blink for $num_blinks times"
        for ((i = 0; i < num_blinks; i++))
        do
            echo 0 > /sys/class/leds/led1/brightness # Turn off
            sleep 0.05
            echo 1 > /sys/class/leds/led1/brightness # Turn on
            sleep 0.05
        done
        echo 0 > /sys/class/leds/led1/brightness # Turn off after short blinks
    else
        echo "Invalid blink type: $blink_type"
        exit 1
    fi
}

# Check for the correct number of arguments
if [ $# -ne 2 ]
then
    echo "Usage: $0 <blink_type> <num_blinks>
blink_type: long short crazy"
    exit 1
fi

blink_type="$1"
num_blinks="$2"

# Set the LED trigger state to "none" before running the script
echo none > /sys/class/leds/led1/trigger

# Call the trigger_led function with the specified arguments
trigger_led "$blink_type" "$num_blinks"

# Set the LED trigger state back to "default-on" after the script is done
echo default-on > /sys/class/leds/led1/trigger
