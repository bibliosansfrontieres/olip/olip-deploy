#!/bin/bash

SLEEPTIME=60 # seconds

get_from_sugar()
{
    local args=$*
    echo "$args" | nc -q 0 127.0.0.1 8423 | cut -d ":" -f2
}

while true; do
    time=$(date '+%H:%M:%S')
    date=$(date '+%F')

    load1=$(cut -d " " -f1 /proc/loadavg)
    load2=$(cut -d " " -f2 /proc/loadavg)
    load3=$(cut -d " " -f3 /proc/loadavg)

    battery_temp=$(get_from_sugar "get temperature")
    cpu_temp=$(sensors -j | jq -r ".[].temp1" | grep temp1 | cut -d ":" -f2)

    battery_level=$(get_from_sugar "get battery")
    battery_v=$(get_from_sugar "get battery_v")
    battery_charing=$(get_from_sugar "get battery_power_plugged")

    con_wifi_ap=$(iw dev wlan0 station dump | grep -c Station)

    echo "$date,$time,$load1,$load2,$load3,$battery_temp,$cpu_temp,$battery_level,$battery_v,$battery_charing,$con_wifi_ap" \
        >> /data/logs/ideascube_mon.log

    sleep "$SLEEPTIME"
done
